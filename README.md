# Rheinturm

Rheinturm is an Arduino appliance for showing the time on a 60 LED strip. It is named
after [the radio tower in Düsseldorf, Germany](https://en.wikipedia.org/wiki/Rheinturm), which is home to the "largest
decimal clock in the world".

## Breadboard layout

![Rheinturm breadboard layout](Rheinturm_bb.png)

## Building

This project uses [PlatformIO](https://platformio.org/). See
the [installation instructions](https://docs.platformio.org/en/latest/core/installation/index.html).

To build, run:

```shell
pio run
```

To upload to the Arduino, run:

```shell
pio run --target upload
```

## Contributing

Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[GNU GPLv3](./LICENSE)

## Acknowledgements

Inspired by the
blogpost ["Arduino Rheinturm - so baust du die größte Uhr der Welt nach"](https://starthardware.org/arduino-rheinturm-lichtzeitpegel/).
