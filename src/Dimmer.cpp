//
// Created by Christian Luijten on 18/06/2023.
//

#include <Arduino.h>
#include "Dimmer.h"


uint8_t Dimmer::updateBrightness()
{
    int brightnessReading = analogRead(pin);
    brightnessAverage += brightnessReading / 4;

    if (brightnessReadings.isFull()) {
        brightnessAverage -= brightnessReadings.shift();
    }

    brightnessReadings.push(brightnessReading / 4);

    return (max(brightnessAverage / brightnessReadings.size(), 1));
}
