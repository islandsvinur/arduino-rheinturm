//
// Created by Christian Luijten on 18/06/2023.
//

#ifndef RHEINTURM_DIMMER_H
#define RHEINTURM_DIMMER_H

#include <CircularBuffer.h>

class Dimmer {
    CircularBuffer<uint8_t, 10> brightnessReadings;
    uint8_t brightnessAverage = 0;
    uint8_t pin;

public:
    Dimmer(uint8_t pin): pin(pin) {}
    uint8_t updateBrightness();
};


#endif //RHEINTURM_DIMMER_H
