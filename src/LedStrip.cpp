//
// Created by Christian Luijten on 18/06/2023.
//

#include "LedStrip.h"

void LedStrip::initialize() {
    pixels.begin();
    pixels.setBrightness(50);
}

void LedStrip::update(const LedStripVisitor &v) {
    for (uint16_t i = 0; i < pixels.numPixels(); ++i) {
        pixels.setPixelColor(i, v.getColor(i));
    }
    pixels.show();
}

void LedStrip::setBrightness(uint8_t i) {
    pixels.setBrightness(i);
}
