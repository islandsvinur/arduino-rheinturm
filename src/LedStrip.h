//
// Created by Christian Luijten on 18/06/2023.
//

#ifndef RHEINTURM_LEDSTRIP_H
#define RHEINTURM_LEDSTRIP_H

#include <Adafruit_Neopixel.h>
#include "LedStripVisitor.h"

class LedStrip {
    Adafruit_NeoPixel pixels;
public:
    LedStrip(uint16_t numPixels, int16_t pin) : pixels(numPixels, pin, NEO_GRB + NEO_KHZ800) {}

    void initialize();

    void update(const LedStripVisitor &v);

    void setBrightness(uint8_t i);
};


#endif //RHEINTURM_LEDSTRIP_H
