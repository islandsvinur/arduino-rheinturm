//
// Created by Christian Luijten on 18/06/2023.
//

#ifndef RHEINTURM_LEDSTRIPVISITOR_H
#define RHEINTURM_LEDSTRIPVISITOR_H

#include <stdint.h>

class LedStripVisitor {
public:
    virtual uint32_t getColor(uint16_t n) const = 0;
};


#endif //RHEINTURM_LEDSTRIPVISITOR_H
