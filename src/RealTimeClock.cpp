//
// Created by Christian Luijten on 18/06/2023.
//

#include "RealTimeClock.h"

#include <AceTime.h>
#include <Wire.h>
#include <SPI.h>

void RealTimeClock::initialize() {
    Wire.begin();
}

ace_time::ZonedDateTime RealTimeClock::getTime() {
    return ace_time::ZonedDateTime::forUnixSeconds64(RTClib::now().unixtime(), timeZone);
}
