//
// Created by Christian Luijten on 18/06/2023.
//

#ifndef RHEINTURM_REALTIMECLOCK_H
#define RHEINTURM_REALTIMECLOCK_H

#include <AceTime.h>
#include <DS3231.h>

class RealTimeClock {
    DS3231 rtc;
    ace_time::BasicZoneProcessor zoneProcessor;
    ace_time::TimeZone timeZone = ace_time::TimeZone::forZoneInfo(&ace_time::zonedb::kZoneEurope_Amsterdam, &zoneProcessor);

public:
    void initialize();

    ace_time::ZonedDateTime getTime();
};


#endif //RHEINTURM_REALTIMECLOCK_H
