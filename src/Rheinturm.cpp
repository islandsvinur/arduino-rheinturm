//
// Created by Christian Luijten on 18/06/2023.
//

#include "Rheinturm.h"

#include <Adafruit_NeoPixel.h>

void Rheinturm::resetLights() {
    for (auto &light: lights) {
        light = OFF;
    }

    lights[26] = RED;
    lights[42] = RED;
}

Rheinturm::Rheinturm() : numLights(NUM_LIGHTS) {
    resetLights();
}

void Rheinturm::setTime(const ace_time::ZonedDateTime &dt) {
    resetLights();

    setDigits(dt.second(), 10);
    setDigits(dt.minute(), 27);
    setDigits(dt.hour(), 44);

    /* Whole Minute and Whole Hour effect */
    if (dt.second() == 59 || dt.minute() == 59) {
        for (int i = 0; i < 10; i++) {
            lights[i] = YELLOW;
        }
        lights[19] = YELLOW;
        lights[25] = YELLOW;
        lights[36] = YELLOW;
        lights[43] = YELLOW;
        lights[53] = YELLOW;
        for (int i = 56; i < 60; i++) {
            lights[i] = YELLOW;
        }
    }
}

void Rheinturm::setDigits(uint8_t amount, int baseIndex) {
    // Ones digit
    for (int i = 0; i < amount % 10; i++) {
        lights[baseIndex + i] = WHITE;
    }
    // Tens digit
    for (int i = 0; i < amount / 10; i++) {
        lights[baseIndex + 10 + i] = WHITE;
    }
}

uint32_t Rheinturm::getColor(uint16_t i) const {
    switch (lights[i]) {
        case YELLOW:
            return Adafruit_NeoPixel::Color(255, 255, 0);
        case RED:
            return Adafruit_NeoPixel::Color(255, 0, 0);
        case WHITE:
            return Adafruit_NeoPixel::Color(255, 255, 255);
        case OFF:
        default:
            return Adafruit_NeoPixel::Color(0, 0, 0);
    }
}

void Rheinturm::printTo(Print& printer) const {
    for (auto &light: lights) {
        switch (light) {
            case YELLOW:
                printer.print('o');
                break;
            case RED:
                printer.print('X');
                break;
            case WHITE:
                printer.print('*');
                break;
            case OFF:
            default:
                printer.print('-');
        }
    }
}
