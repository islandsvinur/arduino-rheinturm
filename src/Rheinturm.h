//
// Created by Christian Luijten on 18/06/2023.
//

#ifndef RHEINTURM_RHEINTURM_H
#define RHEINTURM_RHEINTURM_H

#include <AceTime.h>
#include "LedStripVisitor.h"

#define NUM_LIGHTS 60

enum State {
    OFF,
    WHITE,
    YELLOW,
    RED,
};

class Rheinturm : public LedStripVisitor {
    State lights[NUM_LIGHTS]{};

    void resetLights();

    void setDigits(uint8_t amount, int baseIndex);

public:
    Rheinturm();

    void setTime(const ace_time::ZonedDateTime &);

    uint32_t getColor(uint16_t i) const override;

    const uint16_t numLights;

    void printTo(Print& printer) const;
};

#endif //RHEINTURM_RHEINTURM_H
