#include <Arduino.h>

#include "Rheinturm.h"
#include "LedStrip.h"
#include "RealTimeClock.h"
#include "Dimmer.h"

//#define DEBUG

#define PIN        6
#define DIMMER_PIN A0

Rheinturm rheinturm;
LedStrip ledStrip(rheinturm.numLights, PIN);
RealTimeClock realTimeClock;
Dimmer dimmer(DIMMER_PIN);

void setup() {
#ifdef DEBUG
    Serial.begin(9600);
#endif

    realTimeClock.initialize();
    ledStrip.initialize();
}

void loop() {
    const ace_time::ZonedDateTime &dateTime = realTimeClock.getTime();
    rheinturm.setTime(dateTime);
    ledStrip.setBrightness(dimmer.updateBrightness());
    ledStrip.update(rheinturm);
#ifdef DEBUG
    dateTime.printTo(Serial);
    rheinturm.printTo(Serial);
    Serial.println();
#endif
    delay(100);
}
